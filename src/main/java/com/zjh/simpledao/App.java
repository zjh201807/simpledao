package com.zjh.simpledao;

import com.zjh.simpledao.entity.User;
import com.zjh.simpledao.util.DButil;

/** Hello world! */
public class App {

  public static void main(String[] args) throws Exception {}

  /** 测试删除 */
  public void testDelete() {
    User user = new User();
    user.setId(15L);
    new DButil().deleteById(user);
  }
  /** 测试查询 */
  public void testSelect() {
    User user = new User();
    user.setId(15L);
    System.out.println(new DButil().select(user));
  }
  /** 测试新增方式 */
  public void testInsert() {
    User user = new User();
    user.setUsername("asdafasdfaaa");
    user.setPassword("222dadf");
    new DButil().insert(user);
  }
  /** 测试更新方式 */
  public void testUpdate() {
    User user = new User();
    user.setId(15L);
    user.setUsername("zzzzzzzzzzzzzzz");
    user.setPassword("zjh");
    new DButil().updateById(user);
  }
}
