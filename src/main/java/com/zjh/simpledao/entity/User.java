package com.zjh.simpledao.entity;

import com.zjh.simpledao.annotation.Column;
import com.zjh.simpledao.annotation.Id;
import com.zjh.simpledao.annotation.Table;

/**
 * @author zjh
 * @version V1.0.0
 * @date 2019/2/20 10:18
 * @since 1.0
 */
@Table(name = "user")
public class User {
  @Id(name = "id")
  private Long id;

  @Column(name = "username")
  private String username;

  @Column(name = "password")
  private String password;

  public String getUsername() {
    return username;
  }

  public void setUsername(String username) {
    this.username = username;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  @Override
  public String toString() {
    return "User{"
        + "id="
        + id
        + ", username='"
        + username
        + '\''
        + ", password='"
        + password
        + '\''
        + '}';
  }
}
