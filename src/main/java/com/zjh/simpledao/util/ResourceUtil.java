package com.zjh.simpledao.util;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author zjh
 * @version V1.0.0
 * @date 2019/2/21 14:08
 * @since 1.0
 */
public class ResourceUtil {
  public static Map<String, Map<String, Object>> reflectionCache =
      new ConcurrentHashMap<String, Map<String, Object>>(256);
}
