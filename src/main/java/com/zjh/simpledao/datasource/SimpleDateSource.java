package com.zjh.simpledao.datasource;

import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.SQLFeatureNotSupportedException;
import java.util.ResourceBundle;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.logging.Logger;
import javax.sql.DataSource;

/** ������Դ */
public class SimpleDateSource implements DataSource {
  private static ResourceBundle resourceBundle = ResourceBundle.getBundle("generator");
  private static String dirverClassName;
  private static String url;
  private static String user;
  private static String pswd;
  private static LinkedBlockingDeque<Connection> pool = new LinkedBlockingDeque();
  private static SimpleDateSource instance = new SimpleDateSource();

  static {
    resourceBundle = ResourceBundle.getBundle("generator");
    dirverClassName = resourceBundle.getString("generator.jdbc.driver");
    url = resourceBundle.getString("generator.jdbc.url");
    user = resourceBundle.getString("generator.jdbc.username");
    pswd = resourceBundle.getString("generator.jdbc.password");
  }

  public static SimpleDateSource instance() {
    if (instance == null) {
      instance = new SimpleDateSource();
    }
    return instance;
  }

  @Override
  public Connection getConnection() throws SQLException {
    synchronized (pool) {
      if (pool.size() > 0) {
        return (Connection) pool.removeFirst();
      }
      return makeConnection();
    }
  }

  public static void freeConnection(Connection conn) {
    pool.addLast(conn);
  }

  private Connection makeConnection() throws SQLException {
    return DriverManager.getConnection(url, user, pswd);
  }

  @Override
  public Connection getConnection(String username, String password) throws SQLException {
    return DriverManager.getConnection(url, username, password);
  }

  @Override
  public PrintWriter getLogWriter() throws SQLException {
    return null;
  }

  @Override
  public void setLogWriter(PrintWriter out) throws SQLException {}

  @Override
  public void setLoginTimeout(int seconds) throws SQLException {}

  @Override
  public int getLoginTimeout() throws SQLException {
    return 0;
  }

  @Override
  public Logger getParentLogger() throws SQLFeatureNotSupportedException {
    return null;
  }

  @Override
  public <T> T unwrap(Class<T> iface) throws SQLException {
    return null;
  }

  @Override
  public boolean isWrapperFor(Class<?> iface) throws SQLException {
    return false;
  }

  static {
    try {
      Class.forName(dirverClassName);
    } catch (ClassNotFoundException e) {
      e.printStackTrace();
    }
  }
}
